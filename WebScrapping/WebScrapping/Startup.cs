﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebScrapping.Startup))]
namespace WebScrapping
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
