﻿using AngleSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using WebScrapping.Code;

namespace WebScrapping.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ob result = new ob();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://demowebsracpping.apphb.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // New code:
                HttpResponseMessage response = client.GetAsync("api/WebScrapping").Result;
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<ob>().Result;
                    ViewData["listPost"] = result._listPost;
                    ViewData["listDate"] = result._listDate;
                    ViewData["listLink"] = result._listLink;
                    return View();
                }
            }
           
            // We are only interested in the text - select it with LINQ
            //var titles = cells.Select(m => m.TextContent);
            ViewData["listPost"] = null ;
            ViewData["listDate"] = null;
            ViewData["listLink"] = null;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}