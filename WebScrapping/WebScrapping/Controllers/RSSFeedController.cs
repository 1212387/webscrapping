﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.ServiceModel.Syndication;
using System.Xml;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using AngleSharp;
using WebScrapping.Code;
using System.Net.Http.Headers;
using System.Text;

namespace WebScrapping.Controllers
{
    public class RSSFeedController : ApiController
    {
        [HttpGet]
        [Route("api/WebScrapping")]
        public async Task<IHttpActionResult> webScrappingAPI()
        {
            // Setup the configuration to support document loading
            var config = AngleSharp.Configuration.Default.WithDefaultLoader();
            // Load the names of all The Big Bang Theory episodes from Wikipedia
            var address = "http://www.fit.hcmus.edu.vn/vn/Default.aspx?tabid=53";
            // Asynchronously get the document in a new context using the configuration
            var document = await BrowsingContext.New(config).OpenAsync(address);

            // Perform the query to get all cells with the content
            var Posts = document.QuerySelectorAll("div.post_title a");
            var Dates = document.QuerySelectorAll("div.day_month");
            List<String> listDate = new List<string>();
            List<String> listPost = new List<string>();
            List<String> listLink   = new List<string>();
            Regex regex = new Regex(@"\d{2}\/\d{2}\/\d{4}");
            foreach (var item in Dates)
            {
                MatchCollection matches = regex.Matches(item.InnerHtml);
                listDate.Add(matches[0].ToString());
            }

            foreach (var item in Posts)
            {
                listPost.Add(item.Attributes[1].Value);
                listLink.Add("http://www.fit.hcmus.edu.vn/vn/" + item.Attributes[0].Value);
            }

            var result = new ob() { _listDate = listDate, _listPost = listPost, _listLink = listLink };

            return Ok(result);
        }

        [HttpGet]
        [Route("api/RSS")]
        public  HttpResponseMessage GetFeed()
        {
            ob result = new ob();
            var feed = new SyndicationFeed("RSS Feed Fit KHTN", "This is RSS feed make form fit KHTN", new Uri("http://www.fit.hcmus.edu.vn/vn/Default.aspx?tabid=97"));
            feed.Authors.Add(new SyndicationPerson("beeboo0603@gmail.com"));
            //feed.Categories.Add(new SyndicationCategory("How To Sample Code"));
            feed.Description = new TextSyndicationContent("RSS mục sinh viên lấy từ fit KHTN");

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://demowebsracpping.apphb.com/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // New code:
                HttpResponseMessage response = client.GetAsync("api/WebScrapping").Result;
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsAsync<ob>().Result;

                    List<SyndicationItem> items = new List<SyndicationItem>();
                    for (int i = 0; i < result._listPost.Count; i++)
                    {
                        SyndicationItem item = new SyndicationItem(
                result._listPost[i],
                result._listPost[i],
                new Uri(result._listLink[i]),
                i.ToString(),
                DateTime.Parse(result._listDate[i]));

                        items.Add(item);

                    }
                    feed.Items = items;
                }
                

                
            }


            var formater = new Rss20FeedFormatter(feed);
            var output = new MemoryStream();
            string xml;
            var xws = new XmlWriterSettings { Encoding = Encoding.UTF8 };
            using(var xmlWriter = XmlWriter.Create(output, xws))
            {
                formater.WriteTo(xmlWriter);
                xmlWriter.Flush();
            }

            using (var srr = new StreamReader(output))
            {
                output.Position = 0;
                xml = srr.ReadToEnd();
                srr.Close();
            }
            var Response = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(xml, Encoding.UTF8, "application/rss+xml")
            };
            return Response;
            
        }

    }




}
